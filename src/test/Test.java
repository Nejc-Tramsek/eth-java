package test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.bouncycastle.util.encoders.Hex;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthAccounts;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.exceptions.TransactionException;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Transfer;
import org.web3j.utils.Convert;
import org.web3j.utils.Convert.Unit;

public class Test {
	private static String WALLET1_FILE = "C:\\Workspaces\\eclipse oxygen\\eth-java\\wallet\\UTC--2018-01-26T18-55-47.439000000Z--61465b5688f8b9045d52e26c9405a77b55d3b95d.json";
	private static String WALLET1_PASSWORD = "web3j!test";

	private static String WALLET2_FILE = "C:\\Workspaces\\eclipse oxygen\\eth-java\\wallet\\UTC--2018-01-26T19-47-08.815000000Z--2043124724d0de4ad1b716009ca287f86dfd11a9.json";
	private static String WALLET2_PASSWORD = "web3j!test1";

	public static void main(String[] args) {
		try {
			Web3j web3 = Web3j.build(new HttpService());
			// Wallet1
			Credentials wallet1Credentials = WalletUtils.loadCredentials(WALLET1_PASSWORD, WALLET1_FILE);
			// Wallet2
			Credentials wallet2Credentials = WalletUtils.loadCredentials(WALLET2_PASSWORD, WALLET2_FILE);
			
			test(web3);

			getWalletBalance(web3, wallet1Credentials);

			sendFunds(web3, wallet1Credentials, wallet2Credentials.getAddress(), BigDecimal.valueOf(0.1));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void test(Web3j web3) throws IOException {
		// Test version
		Web3ClientVersion web3ClientVersion = web3.web3ClientVersion().send();
		String clientVersion = web3ClientVersion.getWeb3ClientVersion();
		System.out.println("clientVersion:" + clientVersion);

		// Test observable
		web3.web3ClientVersion().observable().subscribe(x -> {
			String tmp = x.getWeb3ClientVersion();
			System.out.println("clientVersion tmp:" + tmp);
		});

		// Gas price
		EthGasPrice ethGasPrice = web3.ethGasPrice().send();
		System.out.println("ethGasPrice in Wei:" + ethGasPrice.getGasPrice());
		System.out.println("ethGasPrice in ETHER:" + Convert.fromWei(ethGasPrice.getGasPrice().toString(), Unit.ETHER));
	}

	public static void getWalletBalance(Web3j web3, Credentials credentials) throws Exception {
		EthGetBalance ethGetBalance = web3.ethGetBalance(credentials.getAddress(), DefaultBlockParameterName.LATEST)
				.sendAsync().get();

		BigInteger balanceInWei = ethGetBalance.getBalance();
		System.out.println("balanceInWei:" + balanceInWei);
		System.out.println("balanceInETHER:" + Convert.fromWei(balanceInWei.toString(), Unit.ETHER));

	}

	public static void sendFunds(Web3j web3, Credentials from, String to, BigDecimal amountInEther)
			throws InterruptedException, IOException, TransactionException, Exception {
		// Da dela transakcija more bit geth node syncan!
		
		TransactionReceipt transactionReceipt =
		Transfer.sendFunds(web3, from, to, amountInEther, Unit.ETHER).send();
		System.out.println("getTransactionIndex:" + transactionReceipt.getTransactionIndex());
	}

}
